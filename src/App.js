import React from 'react';
import SearchComponent from '../src/components/search-comp';
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  render() {
    return (
      <div className="main-div">
        <SearchComponent />
      </div>
    )
  }
}
export default App;