import React from 'react';
import '../scss/search-comp.scss';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class SearchData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            searchedkey: "",
            searchedData: null,
            headerList: ['S.No', 'Profile', 'Type', 'Name'],
            selectedData: '',
            profileViewData: null,
            profileViewType: false
        }
    }
    contactSubmit = (e) => {
        e.preventDefault();
        if (e.target[0].value === "" || e.target[0].value === undefined) {
            alert("Please enter some text and click search button\nExample: mathivananGuna")
        }
        else {
            if (this.state.searchedkey === e.target[0].value) { }
            else {
                this.setState({ searchedkey: e.target[0].value, searchedData: null }, () => {
                    fetch(`https://api.github.com/search/users?q=${this.state.searchedkey}+in:user`)
                        .then(res => res.json())
                        .then(res => { console.log(res); this.setState({ searchedData: res, profileViewType: false }) })
                        .catch(err => { console.log(err); })
                })
            }
        }
    }
    showDetailList = (e) => {
        this.setState({ selectedData: e.currentTarget.id }, () => {
            const newArray = this.state.searchedData.items.filter(e => Number(e.id) === Number(this.state.selectedData));
            console.log(newArray);
            this.setState({ profileViewData: newArray[0], profileViewType: true })
        })
    }
    goBackFun = (e) => { 
        this.setState({profileViewType:false, profileViewData:null })
     }
    goToRepoFun = (e) => {
        window.open(e.currentTarget.id);
    }
    render() {
        return (
            <div className="search-section-main-div">
                <div className="search-area">
                    <p className="app-label">Github Repository Search Application</p>
                    <form className="example" onSubmit={this.contactSubmit}>
                        <input type="text" size="30" placeholder="Search..." />
                        <button title="click to search"><FontAwesomeIcon icon={faSearch} type="submit" /></button>
                    </form>
                </div>
                <div className="show-searched-list">
                    {this.state.searchedData === null ? <p className="show-message">Click <span>search</span> to find results</p> :
                        <React.Fragment>
                            <p className="show-total">No. of results: <span>{this.state.searchedData.total_count}</span></p>
                            {this.state.profileViewType === false ?
                                <React.Fragment>
                                    <table className="print-details-section">
                                        <thead>
                                            <tr>
                                                {this.state.headerList.map((data, i) => {
                                                    return <td id={i} key={i}>{data}</td>
                                                })}
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.searchedData.items.map((data, i) => {
                                                return <tr title="click to view full details" id={data.id} key={data.id} onClick={(e) => { this.showDetailList(e) }}>
                                                    <td>{i + 1}</td>
                                                    <td><img src={data.avatar_url} alt="Profile_pic" /></td>
                                                    <td className="type">{data.type}</td>
                                                    <td className="profile-name">{data.login}</td>
                                                </tr>
                                            })}
                                        </tbody>
                                    </table>
                                </React.Fragment> :
                                <React.Fragment>
                                    <div className="profile-view-selected">
                                        <div className="image-view">
                                            <img src={this.state.profileViewData.avatar_url} alt='Profile_pic' />
                                        </div>
                                        <div className="person-details">
                                            <p>Name: <span>{this.state.profileViewData.login}</span></p>
                                            <p>Type: <span>{this.state.profileViewData.type}</span></p>
                                            <p>Owner Name: <span>{this.state.profileViewData.login}</span></p>
                                            <p className="listed-data"></p>
                                            <div className="button-section">
                                                <button id={this.state.profileViewData.id} onClick={this.goBackFun}>Bo Back</button>
                                                <button id={this.state.profileViewData.html_url} onClick={this.goToRepoFun}>Go to Github Repository</button>
                                            </div>
                                        </div>
                                        <div className="star-rating">
                                            <FontAwesomeIcon icon={faStar} type="submit" />
                                        </div>
                                    </div>
                                </React.Fragment>
                            }
                        </React.Fragment>
                    }
                </div>
            </div>
        )
    }
}
export default SearchData;